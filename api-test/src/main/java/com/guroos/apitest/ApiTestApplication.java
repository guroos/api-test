package com.guroos.apitest;

import java.security.cert.Certificate;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;

import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.BasicHttpClientConnectionManager;
import org.apache.http.ssl.SSLContexts;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

/**
 * Syntax: ApiTestApplication.jar <TEST|PROD> <username> <password> <apikey>
 * @author micha
 *
 */
@SpringBootApplication
public class ApiTestApplication implements CommandLineRunner{


	private static final Logger log = LoggerFactory.getLogger(ApiTestApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(ApiTestApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		for (int i = 0; i < args.length; ++i) {
			log.info("args[{}]: {}", i, args[i]);
		}
		String loginUrl = null;
		switch (args[0].toUpperCase()) {
		case "TEST":
			loginUrl = "https://test.api.4me.video/login";
			break;
		case "PROD":
			loginUrl = "https://prod.api.4me.video/login";
			break;
		default:
			log.info("Invalid program argument, must be TEST or PROD");
			throw new UnsupportedOperationException();
		}

		
		SSLContext sslContext = SSLContexts.createSystemDefault();
		SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslContext, new HostnameVerifier() {

			@Override
			public boolean verify(String hostname, SSLSession session) {
				log.info("Verifying hostname [" + hostname + "] against:");
				if (session.getLocalCertificates() != null) {
					int i=0;
					for (Certificate c: session.getLocalCertificates()) {
						log.info("local cert " + ++i + ": " + c.toString());
					}
				}
				
				try {
					if (session.getPeerCertificateChain()!=null) {
						int i=0;
						for (javax.security.cert.X509Certificate cert: session.getPeerCertificateChain()) {
							log.info("peer  cert " + ++i + ": " +cert.toString());
						}
					}
				} catch (SSLPeerUnverifiedException e) {
					log.error("Cannot print peer certificates", e);
				}
				
				return true;
			}

		});

		Registry<ConnectionSocketFactory> socketFactoryRegistry = 
				RegistryBuilder.<ConnectionSocketFactory> create()
				.register("https", sslsf)
				.register("http", new PlainConnectionSocketFactory())
				.build();

		BasicHttpClientConnectionManager connectionManager = 
				new BasicHttpClientConnectionManager(socketFactoryRegistry);
		CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(sslsf)
				.setConnectionManager(connectionManager).build();

		HttpComponentsClientHttpRequestFactory requestFactory = 
				new HttpComponentsClientHttpRequestFactory(httpClient);


		LoggingRequestInterceptor interceptor = new LoggingRequestInterceptor();

		Map<String, String> requestbody = new HashMap<String, String>();
		requestbody.put("username", args[1]);
		requestbody.put("password", args[2]);
		final HttpHeaders headers = new HttpHeaders();
		headers.add("x-api-key", args[3]);
		HttpEntity<Map<String, String>> requestEntity = new HttpEntity<>(requestbody, headers);
		RestTemplate rt = new RestTemplate(requestFactory);
		rt.getInterceptors().add(interceptor);
		rt.setErrorHandler(interceptor);
		ResponseEntity<String> postForEntity = rt.exchange(loginUrl, HttpMethod.POST, requestEntity, String.class);

	}

}
