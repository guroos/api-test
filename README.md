# 4me-video api-test

## Purpose

To test certificate chains in use

## Syntax

`api-test-0.0.1-SNAPSHOT.jar <TEST|PROD> <username> <password> <apikey>`

for example 

`api-test-0.0.1-SNAPSHOT.jar PROD dxcus secretpassword a4d4rg4vf54s4sd4v4r4g4r4ag`